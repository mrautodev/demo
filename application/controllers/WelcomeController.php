<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WelcomeController extends MY_Controller {
	
	public function index()
	{		
		$this->TamplatePublicView('auth/index_login', '', '');
	}
}