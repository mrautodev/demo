<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends MY_Controller {
		
    public function __construct()
	{
		parent::__construct();
        $this->load->model('User_model');
	}
    
    public function login()
    {        
        $this->TamplatePublicView('auth/index_login', '', '');    
    }

    public function validlogin()
    {
        if($this->input->server('REQUEST_METHOD') == TRUE){
            // $result = $this->User_model->fetch_user_login($this->input->post('username'), $this->input->post('password'));
            
			// $this->session->set_userdata(
            //     array(
            //         'logged_id'    => $result->user_id,
            //         'display_name'=> $result->name
            //         )
            // );

            if($this->input->post('username') == ''){
                msg_notify('error', 'ชื่อผู้ใช้หรือรหัสผ่านผิดพลาด !!!');
                redirect('login');
            }else{
                msg_notify('success', 'เข้าสู่ระบบเรียบร้อย');
                redirect('admin/dashboard');
            }
            
			
        }
    }

    public function logout()
    {        
        $this->session->unset_userdata(array('logged_id'));
		redirect('', 'refresh');
    }
}