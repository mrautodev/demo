<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SaleController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'ขายสินค้า';        
		$this->TemplateAdminView('admin/sale/index_sale', '', $this->data);		
	}
}