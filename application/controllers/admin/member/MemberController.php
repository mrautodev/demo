<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MemberController extends MY_Controller {
	
	public function index()
	{
        $this->data['title'] = 'ข้อมูลผู้ใช้งาน';        
		$this->TemplateAdminView('admin/member/index_member', '', $this->data);
	}
}