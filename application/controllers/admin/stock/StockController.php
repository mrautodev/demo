<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StockController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'สต็อคสินค้า';
        
		$this->TemplateAdminView('admin/stock/index_stock', 'admin/stock/script_stock', $this->data);		
	}
}