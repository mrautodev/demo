<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QuotationController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'ใบเสนอราคา';        
		$this->TemplateAdminView('admin/quotation/index_quotation', '', $this->data);		
	}

	public function form_quotation()
	{
		$this->data['title'] = 'แบบฟอร์มใบเสนอราคา';        
		$this->TemplateAdminView('admin/quotation/form_quotation', 'admin/quotation/script_form', $this->data);
	}
}