<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RentalController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'ใบกำกับการเช่า';        
		$this->TemplateAdminView('admin/rental/index_rental', '', $this->data);		
	}
}