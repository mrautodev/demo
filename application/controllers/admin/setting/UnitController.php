<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UnitController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'หน่วยนับ';       
		$this->TemplateAdminView('admin/setting/unit/index_unit', '', $this->data);
	}
}