<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'หมวดหมู่';       
		$this->TemplateAdminView('admin/setting/category/index_category', '', $this->data);
	}
}