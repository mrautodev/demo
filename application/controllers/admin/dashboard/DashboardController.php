<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'รายงาน';		
		$this->TemplateAdminView('admin/dashboard/index_dashboard', '', $this->data);		
	}
}