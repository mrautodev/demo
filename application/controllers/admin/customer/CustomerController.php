<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerController extends MY_Controller {
	
	public function index()
	{
		$this->data['title'] = 'รายชื่อลูกค้า';
        
		$this->TemplateAdminView('admin/customer/index_customer', '', $this->data);	
	}
}