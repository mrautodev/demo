<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderController extends MY_Controller {
	
	public function index()
	{
        $this->data['title'] = 'รายการสั่งซื้อ';
        
		$this->TemplateAdminView('admin/order/index_order', '', $this->data);		
	}
}