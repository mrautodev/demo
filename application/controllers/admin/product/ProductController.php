<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{					
		$this->data['data'] = $this->product_model->get_AllProduct();		
		
		$this->data['title'] = 'รายการสินค้า';       
		$this->TemplateAdminView('admin/product/index_product', 'admin/product/script_product', $this->data);		
	}

	public function form_product()
	{		
		$this->data['title'] = 'รายการสินค้าเพิ่มข้อมูล';        
			
		$this->TemplateAdminView('admin/product/form_product', '', $this->data);
	}

	public function edit_product()
	{
		$this->data['title'] = 'รายการสินค้าแก้ไขข้อมูล';       
			
		$this->TemplateAdminView('admin/product/edit_product', '', $this->data);
	}	
}