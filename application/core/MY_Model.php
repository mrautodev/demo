<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public function My_Insert($table, $object)
    {
        $this->db->insert($table, $object);
        return $this->db->insert_id();
    }

    public function My_Select($table, $object)
    {
        $query = $this->db->get_where($table,  $object);
        return $query->result();
    }

    public function My_Update($field, $id, $table, $object)
    {
        $this->db->where($field, $id);
        $this->db->update($table, $object);
               
        if($this->db->affected_rows() > 0 ){
            return 1;
        }else{
            return 0;
        }
    }

    public function My_Delete($table, $object)
    {
        $this->db->delete($table, $object);
        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
        
}