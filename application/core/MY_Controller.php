<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $user_id;   

    protected $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('logged_id') ? $this->session->userdata('logged_id') : '';
    }
	
	public function TemplateAdminView($page, $script, $data)
	{
		$this->load->view('template/backend/header', $data);
		$this->load->view('template/backend/navbar', $data);
		$this->load->view('template/backend/sidebar', $data);

		$this->load->view($page, $data);		
		
		$this->load->view('template/backend/footer', $data);
		
		if($script != ''){
			$this->load->view($script, $data);
		}
		
	}

	public function TamplatePublicView($page, $script, $data)
	{
		$this->load->view('template/fontend/header');
        $this->load->view($page);  
        $this->load->view('template/fontend/footer');

		if($script != ''){
			$this->load->view($script, $data);
		}
	}
}