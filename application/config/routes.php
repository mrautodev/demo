<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/**
 * url login
 */
$route['login'] = 'Auth/LoginController/login';
$route['auth'] = 'Auth/LoginController/validlogin';
$route['logout'] = 'Auth/LoginController/logout';

$route['register'] = 'Auth/RegisterController/register';

/**
 * url admin view
 */
$route['admin/dashboard'] = 'admin/dashboard/DashboardController';

$route['admin/sale'] = 'admin/sale/SaleController';

$route['admin/product'] = 'admin/product/ProductController';
$route['admin/product/form'] = 'admin/product/ProductController/form_product';

$route['admin/product/get'] = 'admin/product/ProductController/getAllProduct';

$route['admin/stock'] = 'admin/stock/StockController';
$route['admin/order'] = 'admin/order/OrderController';



$route['admin/customer'] = 'admin/customer/CustomerController';

$route['admin/quotation'] = 'admin/quotation/QuotationController';
$route['admin/quotation/form'] = 'admin/quotation/QuotationController/form_quotation';


$route['admin/rental'] = 'admin/rental/RentalController';
$route['admin/receipt'] = 'admin/receipt/ReceiptController';

/**
 * url Setting
 */
$route['admin/category'] = 'admin/setting/CategoryController';

$route['admin/unit'] = 'admin/setting/UnitController';

$route['admin/member'] = 'admin/member/MemberController';