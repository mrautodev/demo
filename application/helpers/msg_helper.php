<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function message_show($type, $text){
    $ci =& get_instance();
    $ci->session->set_flashdata(array(
        'msgerr'=> '<div id="msg" style="top:10px;position:absolute;display:flex;justify-content: center;width:100%;z-index:999;"><div class="alert alert-'.$type.'" style="width:80%;text-align:center;">'.$text.'</div></div>'
    ));
}

function msg_notify($type, $text)
{
    $ci =& get_instance();
    $ci->session->set_flashdata(array(
        'msgerr'=> '<script>toastr.'.$type.'("'.$text.'", {timeOut: 2000});</script>',
    ));    
}