<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

    public function add_user($data)
    {       
		$this->db->insert('tb_user', $data);        
        return $this->db->insert_id();
    }
    
	public function fetch_user_login($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$this->salt_pass($password));
		$query = $this->db->get('tb_user');
		return $query->row();
	}
	 
	public function salt_pass($password)
	{
		return md5($password);
	}
 
	public function read_user($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('tb_admin');
		if($query->num_rows() > 0){
			$data = $query->row();
			return $data;
		}
		return FALSE;
	}	
}