<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	}

    public function get_AllProduct()
    {
        $this->db->select('*');
        $this->db->from('tb_product');
        $this->db->order_by('p_id', 'DESC');        
        $query = $this->db->get();
        $i = 1;
		$data_json = array();
        if($query->num_rows() > 0){
            foreach($query->result_array() as $row){
                $json = array(
                    'order' => $i++,
                    'barcode' => $row['p_barcode'],
                    'image' => $row['p_image'],
                    'name' => $row['p_name'],
                    'type' => $row['p_type'],
                    'cost' => $row['p_cost'],
                    'price' => $row['p_price'],                    
                    'active' => $row['p_active'],
                    'detail' => $row['p_id'],				
                );

                array_push($data_json, $json);
            }
            return $data_json;
        }else{
            return 0;
        }
    }
}