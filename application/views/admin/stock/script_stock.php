<script>
$(document).ready(function() {
    $('#stock_table').DataTable({
        "pageLength": 10,
        "oLanguage": {
            "sLengthMenu": "แสดง _MENU_ แถว ต่อหน้า",
            "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
            "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ แถว",
            "sInfoEmpty": "",
            "sInfoFiltered": "",
            "sSearch": "ค้นหา :",
            "oPaginate": {
                "sNext": "ถัดไป",
                "sPrevious": "กลับ",
            }
        },
        "lengthChange": false,
        "searching": true,
        "bSort": false,
        'sDom': 'lrtip',
    });
});
</script>