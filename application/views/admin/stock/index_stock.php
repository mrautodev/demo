<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <i class="nav-icon fas fa-cart-arrow-down mr-2"></i>สต็อคสินค้า
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fas fas fa-cart-arrow-down"></i>
                        ข้อมูลสต็อคสินค้า
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo base_url('admin/product/form-product'); ?>" class="btn btn-primary"><i
                                    class="fas fa-plus mr-2"></i>เพิ่มสินค้า</a>
                        </div>
                    </div>

                    <div class="table-responsive mt-3">
                        <table id="stock_table" class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col" style="width: 3%;">ลำดับ</th>
                                    <th scope="col" style="width: 10%;">รหัสสินค้า</th>
                                    <th scope="col" style="width: 10%;">รูปสินค้า</th>
                                    <th scope="col" style="width: 35%;">ชื่อสินค้า</th>
                                    <th scope="col" style="width: 15%;">หมวดหมู่</th>
                                    <th scope="col" style="width: 10%;">คงเหลือ</th>
                                    <th scope="col" style="width: 7%;">สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">1</td>
                                    <td scope="row">P00001</td>
                                    <td scope="row">รูปสินค้า</td>
                                    <td scope="row">เบียร์ช้าง</td>
                                    <td scope="row">0</td>
                                    <td scope="row">
                                        2
                                        <div class="btn-group btn-group-sm shadow float-right" role="group"
                                            aria-label="First group">
                                            <button type="button" class="btn btn-success">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td scope="row" class="text-center">
                                        <small class="badge badge-warning text-wrap">
                                            สินค้าใกล้หมด
                                        </small>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>