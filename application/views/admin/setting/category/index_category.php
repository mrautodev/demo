<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <h6 class="m-0 text-dark"><i class="nav-icon fas fa-shopping-cart mr-2"></i>หมวดหมู่สินค้า
                            </h6>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fas fa-shopping-cart"></i>
                        ข้อมูลหมวดหมู่สินค้า
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo base_url('admin/category/form'); ?>" class="btn btn-primary"><i
                                    class="fas fa-plus mr-2"></i>เพิ่มข้อมูล</a>
                        </div>
                    </div>

                    <fieldset class="border p-2">
                        <legend class="w-auto">ค้นหา</legend>
                        <div class="form-row">
                            <div class="col-xs-12 col-sm-12 col-md-8 mt-2">
                                <input type="text" class="form-control" placeholder="ชื่อหมวดหมู่" name="name_category"
                                    id="name_category">
                            </div>
                            <div class="col-xs-6 col-md-2 mt-2">
                                <button class="btn btn-primary btn-block">ค้นหา</button>
                            </div>
                            <div class="col-xs-6 col-md-2 mt-2">
                                <button class="btn btn-success btn-block">แสดงทั้งหมด</button>
                            </div>
                        </div>
                    </fieldset>

                    <div class="table-responsive mt-3">
                        <table id="product_table" class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col" style="width: 3%;">ลำดับ</th>
                                    <th scope="col" style="width: 87%;">ชื่อหมวดหมู่</th>
                                    <th scope="col" style="width: 10%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>