<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <i class="nav-icon fas fa-shopping-cart mr-2"></i>ผู้ดูแลระบบ
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fas fa-shopping-cart"></i>
                        ข้อมูลผู้ดูแลระบบ
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo base_url('admin/product/form-product'); ?>" class="btn btn-primary"><i
                                    class="fas fa-plus mr-2"></i>เพิ่มข้อมูล</a>
                        </div>
                    </div>

                    <div class="table-responsive mt-3">
                        <table id="product_table" class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col" style="width: 3%;">ลำดับ</th>
                                    <th scope="col" style="width: 12%;">รหัสพนักงาน</th>
                                    <th scope="col" style="width: 10%;">รูปพนักงาน</th>
                                    <th scope="col" style="width: 30%;">ชื่อ-นามสกุล</th>
                                    <th scope="col" style="width: 20%;">ตำแหน่ง</th>
                                    <th scope="col" style="width: 10%;">สถานะ</th>
                                    <th scope="col" style="width: 15%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>