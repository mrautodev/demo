<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <i class="nav-icon fas fa-shopping-cart mr-2"></i>รายการสินค้า
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fas fa-shopping-cart"></i>
                        ข้อมูลรายการสินค้า
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo base_url('admin/product/form'); ?>" class="btn btn-primary"><i
                                    class="fas fa-plus mr-2"></i>เพิ่มสินค้า</a>
                        </div>
                    </div>

                    <div class="table-responsive mt-3">
                        <table id="product_table" class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col" style="width: 3%;">ลำดับ</th>
                                    <th scope="col" style="width: 11%;">รหัสสินค้า</th>
                                    <th scope="col" style="width: 10%;">รูปสินค้า</th>
                                    <th scope="col" style="width: 32%;">ชื่อสินค้า</th>
                                    <th scope="col" style="width: 15%;">หมวดหมู่</th>
                                    <th scope="col" style="width: 7%;">ต้นทุน</th>
                                    <th scope="col" style="width: 7%;">ราคาขาย</th>
                                    <th scope="col" style="width: 5%;">สถานะ</th>
                                    <th scope="col" style="width: 10%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($data as $list){?>
                                <tr>
                                    <td scope="row"><?php echo $list['order'];?></td>
                                    <td scope="row"><?php echo $list['barcode'];?></td>
                                    <td scope="row" class="text-center"><?php echo $list['image'];?></td>
                                    <td scope="row"><?php echo $list['name'];?></td>
                                    <td scope="row"><?php echo $list['type'];?></td>
                                    <td scope="row" class="text-center"><?php echo $list['cost'];?></td>
                                    <td scope="row" class="text-center"><?php echo $list['price'];?></td>
                                    <td scope="row" class="text-center">
                                        <input type="checkbox" checked data-toggle="toggle" data-size="xs" data-on="ขาย"
                                            data-off="ปิด" data-onstyle="success" data-offstyle="danger"
                                            id="toggle-event">
                                    </td>
                                    <td scope="row" class="text-center">
                                        <div class="btn-group btn-group-sm shadow" role="group"
                                            aria-label="First group">
                                            <button type="button" class="btn btn-info">
                                                <i class="fas fa-search"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>