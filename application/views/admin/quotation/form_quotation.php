<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url('admin/quotation'); ?>"><i
                                    class="fas fa-edit mr-2"></i>ใบเสนอราคา
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <i class="fas fa-paste mr-2 mt-1"></i>แบบฟอร์มใบเสนอราคา
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fas fa-paste"></i>
                        แบบฟอร์มใบเสนอราคา
                    </h4>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#exampleModal"><i class="fas fa-plus mr-2"></i>เพิ่มรายการ</button>
                        </div>
                    </div>
                    <table class="table table-bordered mt-3">
                        <thead>
                            <tr>
                                <th style="width: 15%;" class="text-center">รหัสสินค้า</th>
                                <th style="width: 35%;" class="text-center">รายการสินค้า</th>
                                <th style="width: 10%;" class="text-center">จำนวน</th>
                                <th style="width: 10%;" class="text-center">หน่วย</th>
                                <th style="width: 10%;" class="text-center">ราคา/หน่วย</th>
                                <th style="width: 10%;" class="text-center">ส่วนลด</th>
                                <th style="width: 10%;" class="text-center">จำนวนเงิน</th>
                            </tr>
                        </thead>
                        <tbody id="tb_body">

                        </tbody>
                    </table>

                    <table class="table table-bordered">
                        <tr>
                            <td rowspan="4" style="width: 60%;" class="pt-3">
                                <label for="">หมายเหตุ</label>
                                <textarea name="note" rows="7" class="form-control" style="resize: none;"></textarea>
                            </td>
                            <td style="width: 15%;" class="text-right">รวมเงิน :</td>
                            <td style="width: 25%;">
                                <input type="text" name="total" class="form-control form-control-sm">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">ส่วนลดการค้า :</td>
                            <td>
                                <input type="text" name="discount" class="form-control form-control-sm">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">เงินหลังหักส่วนลด :</td>
                            <td>
                                <input type="text" name="after_discount" class="form-control form-control-sm">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">ภาษีมูลค่าเพิ่ม :</td>
                            <td>
                                <input type="text" name="vat" class="form-control form-control-sm">
                            </td>
                        </tr>
                        <tr>
                            <td>เงินภาษาไทย</td>
                            <td class="text-right">จำนวนเงินทั้งสิ้น :</td>
                            <td>
                                <input type="text" name="amount" class="form-control form-control-sm">
                            </td>
                        </tr>
                    </table>



                    <table class="table table-bordered">
                        <tr>
                            <td style="width: 33.33%;">
                                <label for="">ผู้อนุมัติซื้อ</label>
                                <input type="text" name="customer_name" class="form-control">
                                <div class="input-group mt-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    <input type="text" class="form-control" name="date_customer"
                                        placeholder="วัน/เดือน/ปี" readonly>
                                </div>
                            </td>
                            <td style="width: 33.33%;">
                                <label for="">พนักงานขาย</label>
                                <input type="text" name="sale_name" class="form-control">
                                <div class="input-group mt-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    <input type="text" class="form-control" name="date_sale" placeholder="วัน/เดือน/ปี"
                                        readonly>
                                </div>
                            </td>
                            <td style="width: 33.33%;">
                                <label for="">ผู้จัดการฝ่ายขาย</label>
                                <input type="text" name="manager_name" class="form-control">
                                <div class="input-group mt-3">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    <input type="text" class="form-control" name="date_manager"
                                        placeholder="วัน/เดือน/ปี" readonly>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>