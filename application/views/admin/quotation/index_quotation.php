<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <i class="nav-icon fas fa-edit mr-2"></i>ใบเสนอราคา
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fas fa-edit"></i>
                        ข้อมูลใบเสนอราคา
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo base_url('admin/quotation/form'); ?>" class="btn btn-primary"><i
                                    class="fas fa-plus mr-2"></i>เพิ่มใบเสนอราคา</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>