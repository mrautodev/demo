<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="row mb-2 pt-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <i class="nav-icon fa fa-cart-plus mr-2"></i>รายการสั่งซื้อ
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card shadow">
                <div class="card-header border-0 pt-4">
                    <h4>
                        <i class="fa fa-cart-plus"></i>
                        ข้อมูลรายการสั่งซื้อ
                    </h4>
                </div>
                <div class="card-body">

                    <div class="table-responsive mt-3">
                        <table id="product_table" class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col" style="width: 3%;">ลำดับ</th>
                                    <th scope="col" style="width: 20%;">เลขที่บิล</th>
                                    <th scope="col" style="width: 20%;">ยอดรวม</th>
                                    <th scope="col" style="width: 20%;">ผู้บันทึกรายการ</th>
                                    <th scope="col" style="width: 10%;">วันที่สร้างรายการ</th>
                                    <th scope="col" style="width: 7%;">สถานะ</th>
                                    <th scope="col" style="width: 10%;">จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>