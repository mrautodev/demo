<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web-App</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-bs4/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/toastr/toastr.css">
    <link rel="stylesheet"
        href="<?php echo base_url();?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit" rel="stylesheet">


    <style>
    html,
    body {
        height: 100%;
        font-family: 'Kanit', sans-serif;
    }

    .bg {
        background: #7C8BFF;
        width: 100%;
        height: 100%;
        position: absolute;
        z-index: -1;
        clip-path: polygon(0 0, 55% 0, 45% 100%, 0% 100%);
        /*background: linear-gradient(180deg, #BADFFF 50%, #FFEBE0 50%);*/
    }

    .masthead {
        height: 40vh;
        min-height: 500px;
        background-image: url('<?php echo base_url('uploads/cover/default.jpg');?>');
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
    </style>
</head>

<body>