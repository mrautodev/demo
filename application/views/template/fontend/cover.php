<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-end">
            <div class="col-12 text-center pb-4">
                <?php $getProfile = getprofile(1);?>
                <img src="<?php echo base_url('uploads/avatar/').$getProfile['avatar'];?>"
                    style="border:5px solid #FFFFFF;" class="rounded-circle shadow-lg" alt="Cinque Terre" width="140"
                    height="140">
            </div>
        </div>
    </div>
</header>