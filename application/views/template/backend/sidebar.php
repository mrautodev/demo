<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url('uploads/avatar/default.png') ?>" alt=""
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE Auto DEv</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?php echo base_url('uploads/avatar/default.png') ?>" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Mr.Auto DEv</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                    <a href="<?php echo base_url('admin/dashboard'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='dashboard' ? 'active':''?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            หน้ารายงาน
                            <!-- <i class="right fas fa-angle-left"></i> -->
                        </p>
                    </a>
                    <!-- <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Active Page</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Inactive Page</p>
                            </a>
                        </li>
                    </ul> -->
                </li>

                <!-- <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>
                            ผู้ใช้
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>
                            รูปแบบ
                        </p>
                    </a>
                </li> -->
                <li class="nav-header">ระบบสินค้า</li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/sale'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='sale' ? 'active':''?>">
                        <i class="nav-icon fas fa-store"></i>
                        <p>
                            ขายสินค้า
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/order'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='order' ? 'active':''?>">
                        <i class="nav-icon fa fa-cart-plus"></i>
                        <p>
                            รายการสั่งซื้อ
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/product'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='product' ? 'active':''?>">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            รายการสินค้า
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/stock'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='stock' ? 'active':''?>">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>
                            สต็อคสินค้า
                        </p>
                    </a>
                </li>

                <li class="nav-header">ระบบลูกค้า</li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/customer'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='customer' ? 'active':''?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            รายชื่อลูกค้า
                        </p>
                    </a>
                </li>

                <!-- <li class="nav-header">ระบบออกใบส่งสินค้า</li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/delivery'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='delivery' ? 'active':''?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            รายการสินค้าที่ต้องส่ง
                        </p>
                    </a>
                </li> -->

                <li class="nav-header">ระบบจัดการ</li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/quotation'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='quotation' ? 'active':''?>">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            ใบเสนอราคา
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo base_url('admin/rental'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='rental' ? 'active':''?>">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            ใบกำกับการเช่า-คืน
                        </p>
                    </a>
                </li>

                <li class="nav-header">ผู้ใช้งาน</li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/member'); ?>"
                        class="nav-link <?php echo $this->uri->segment(2)==='member' ? 'active':''?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            ข้อมูลผู้ใช้งาน
                        </p>
                    </a>
                </li>

                <li class="nav-header">ตั้งค่าระบบ</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            ระบบสินค้า
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('admin/category'); ?>"
                                class="nav-link <?php echo $this->uri->segment(2)==='category' ? 'active':''?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>หมวดหมู่สินค้า</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('admin/unit'); ?>"
                                class="nav-link <?php echo $this->uri->segment(2)==='unit' ? 'active':''?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>หน่วยนับ</p>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>