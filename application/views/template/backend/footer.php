<footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 Mr.Auto DEv.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
</footer>
</div>
<script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/popper/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-bs4/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/adminLTE-3/js/adminlte.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap4-toggle/js/bootstrap4-toggle.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables-bs4/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<?php echo $this->session->flashdata('msgerr')?>
</body>

</html>