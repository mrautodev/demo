<header class="bg"></header>
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-5">
            <div class="card border-0 shadow-lg">
                <div class="card-header border-0">
                    <h3>เข้าสู่ระบบ</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo site_url('auth'); ?>" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">ชื่อผู้ใช้</label>
                            <input type="text" class="form-control" name="username" required
                                oninvalid="this.setCustomValidity('กรุณากรอกชื่อผู้ใช้ของท่าน')"
                                oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">รหัสผ่าน</label>
                            <input type="password" class="form-control" name="password" required
                                oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่านของท่าน')"
                                oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-check mb-4">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>